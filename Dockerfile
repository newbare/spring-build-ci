FROM maven:3-jdk-8

ENV JAR_REL_LOCATION=default.jar

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ONBUILD ADD . /usr/src/app
ONBUILD RUN mvn clean install
ONBUILD RUN mvn package

EXPOSE 80

CMD java -Dspring.profiles.active=production -Xmx512m -jar "target/$JAR_REL_LOCATION"